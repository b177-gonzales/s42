const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFirstName = document.querySelector('#span-first-name');
const spanLastName = document.querySelector('#span-last-name');

// Alternative ways to retrieve elements
// document.getElementById('txt-first-name');
// document.getElementsByClassName('txt-inputs');
// document.getElementsByTagName('input');

txtFirstName.addEventListener('keyup', (event) => {
	spanFirstName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener('keyup', (event) => {
	spanLastName.innerHTML = txtLastName.value;
})

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
})

txtLastName.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
})